import React from 'react';
import ProductList from '../../components/productList';

const MainScreen = (props) => {
	return (
        <ProductList products={props.products} onAddToCart={props.onAddToCart} onAddToFavorites={props.onAddToFavorites}/>
	);
};

export default MainScreen;