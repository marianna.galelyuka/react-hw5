import React from 'react';
import Item from '../../components/item';

const FavoritesScreen = ({ favorites }) => {
	return (
        <div>
            <h2>Your favorites</h2>
            {favorites.length ? (
            <div>
                {favorites.map((el) => {
                    return (
                        <Item key={el.id} item={el} isCloseIcon={false}/>
                    )
                })}
            </div>) : <p style={{ textAlign: "center" }}>You have no favorites yet :(</p>}
        </div>
	);
};

export default FavoritesScreen;