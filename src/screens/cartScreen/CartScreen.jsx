import React from 'react';
import Item from '../../components/item';
import CustomerForm from '../../components/form';

const CartScreen = ({ cartItems, onDeleteFromCart, onClearCart }) => {

    const calculateTotal = (Arr) => {
        let totalSum = 0;
        Arr.forEach((el) => {
            totalSum += Number.parseFloat(el.price)
        })
        return totalSum.toFixed(3)
    }

    return (
        <div>
            <h2>Your cart</h2>
            {cartItems.length ? (
            <div>
                {cartItems.map((el) => (
                    <Item key={el.id} item={el} isCloseIcon={true} onDeleteFromCart={onDeleteFromCart}/>
                ))}
                <b style={{display: "block", textAlign: "center", margin: "36px 0px 60px 0px"}}>
                    Total: UAH {calculateTotal(cartItems)}
                </b>
                <CustomerForm cart={cartItems} onClearCart={onClearCart}/>
            </div>) : <p style={{ textAlign: "center" }}>You cart is empty!</p>
            }
        </div>
    );
}

export default CartScreen;