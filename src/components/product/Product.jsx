import React, { useState } from 'react';
import Button from '../button';
import Modal from '../modal';
import PropTypes from 'prop-types';

import { FaStar } from "react-icons/fa";

import styles from "./Product.module.css";

const Product = (props) => {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [isFavorite, setIsFavorite] = useState(false);

    const changeModal = () => {
        setIsModalOpen(!isModalOpen);
    }

    const changeFavorite = () => {
        setIsFavorite(!isFavorite);
        props.product.isFavorite ? props.product.isFavorite = false : props.product.isFavorite = true;
        props.onAddToFavorites(props.product);
    }

    const addToCart = () => {
        setIsModalOpen(!isModalOpen)
        props.onAddToCart(props.product);
    }

    return (
        <div className={styles.product}>
            <img className={styles.product__img} src={props.product.imgUrl} alt="Product" />
            <FaStar id={props.product.id} onClick={changeFavorite} className={`${styles.product__favIcon} ${props.product.isFavorite && `${styles.active}`}`}  />
            <h4 className={styles.product__title}>{props.product.productName}</h4>
            <div className={styles.product__details}>
                <p className={styles.product__details_color}> {props.product.color}</p>
                <p className={styles.product__details_price}> UAH {props.product.price}</p>
            </div>
            <Button text="Add to cart" className={styles.product__toCartBtn} onClick={changeModal} />

            {isModalOpen && (
                <div className={styles.modal} onClick={changeModal}>
                    <Modal header="Do you want to add this product to a cart?" text="Adding a product to a cart doesn't mean its reservation. Do you want to continue?" actions={<><Button text='Ok' className={styles.product__toCartBtn_confirm} onClick={addToCart}/><Button text="Cancel" className={styles.product__toCartBtn_cancel} onClick={changeModal} isOpen={isModalOpen} /></>} />
                </div>
            )}
        </div>
    )
}

Product.propTypes = {
    isFavorite: PropTypes.bool,
    product: PropTypes.object,
    imgUrl: PropTypes.string,
    id: PropTypes.number,
    productName: PropTypes.string,
    color: PropTypes.string,
    price: PropTypes.number,
};

export default Product;