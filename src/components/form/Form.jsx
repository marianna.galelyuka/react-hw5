import React from 'react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
// import { PatternFormat } from 'react-number-format';

import styles from "./Form.module.css";

const SignupSchema = Yup.object().shape({
    firstName: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    lastName: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    age: Yup.number().required('Required'),
    address: Yup.string()
        .min(10, "Please enter your full adress")
        .required('Required'),
    phoneNumber: Yup.string().length(14, 'Please enter correct phone number').required('Required'),
});

const CustomerForm = (props) => (
  <div className={styles.form}>
    <h4 style={{textAlign: 'center', margin: '24px 0px', color: 'rgb(50, 150, 150)'}}>If you are ready, please complete your order :)</h4>
    <Formik
        initialValues={{
            firstName: '',
            lastName: '',
            age:'',
            address: '',
            phoneNumber: '',
        }}
        validationSchema={SignupSchema}
        onSubmit={values => {
            console.log('order', props.cart, 'customer', values);
            props.onClearCart();
        }}
    >
      {({ errors, touched }) => (
        <Form className={styles.form__content}>
            <label htmlFor="firstName">First name:</label>
            <Field name="firstName" />
            {errors.firstName && touched.firstName ? (
                <div>{errors.firstName}</div>
            ) : null}

            <label htmlFor="lastName">Last name:</label>
            <Field name="lastName" />
            {errors.lastName && touched.lastName ? (
                <div>{errors.lastName}</div>
            ) : null}

            <label htmlFor="age">Age:</label>
            <Field name="age" />
            {errors.age && touched.age ? (
                <div>{errors.age}</div>
            ) : null}

            <label htmlFor="address">Address:</label>
            <Field name="address" />
            {errors.address && touched.address ? (
                <div>{errors.address}</div>
            ) : null}

            <label htmlFor="phoneNumber">Phone number (in the format of (###) ###-####):</label>
            <Field name="phoneNumber" />
            {errors.phoneNumber && touched.phoneNumber ? <div>{errors.phoneNumber}</div> : null}
            {/* <PatternFormat name="phoneNumber" format="(###) ###-####" mask="#" required/> */}

            <button type="submit" className={styles.form__submitBtn}>Checkout</button>
        </Form>
      )}
    </Formik>
  </div>
);

export default CustomerForm;


// import React from 'react';
// import { Formik, Form, Field } from 'formik';

// import styles from "./Form.module.css";

// const validateName = (value) => {
//     if (!value) {
//         return 'Required'
//     } else if (value && value.length < 2 ) {
//         return  "Too short"
//     } else if (value && value.length > 30) {
//         return  "Too long"
//     }
// }

// const validateAge = (value) => {
//     if (!value) {
//         return 'Required'
//     } else if (value && isNaN(value)) {
//         return  "Invalid age"
//     }
// }

// const validateAddress = (value) => {
//     if (!value) {
//         return 'Required'
//     } else if (value && value.length < 10) {
//         return  "Invalid addredd"
//     }
// }

// const validatePhone = (value) => {
//     if (!value) {
//         return 'Required'
//     } else if (value && value.length < 10) {
//         return  "Invalid phone number"
//     }
// }

// const CustomerForm = (props) => {
//     return (
//         <div className={styles.form}>
//             <Formik
//                 initialValues={{
//                     firstName: '',
//                     lastName: '',
//                     age:'',
//                     address: '',
//                     phoneNumber: '',
//                 }}
//                 onSubmit={values => {
//                     console.log('order', props.cart, 'customer', values);
//                     props.onClearCart();
//                 }}
//             >
//                 {({ errors, touched }) => (
//                     <Form className={styles.form__content}>
//                         <label htmlFor="firstName">First name:</label>
//                         <Field name="firstName" validate={validateName} />
//                         {errors.firstName && touched.firstName ? (
//                             <div>{errors.firstName}</div>
//                         ) : null}
            
//                         <label htmlFor="lastName">Last name:</label>
//                         <Field name="lastName" validate={validateName} />
//                         {errors.lastName && touched.lastName ? (
//                             <div>{errors.lastName}</div>
//                         ) : null}
            
//                         <label htmlFor="age">Age:</label>
//                         <Field name="age" validate={validateAge} />
//                         {errors.age && touched.age ? (
//                             <div>{errors.age}</div>
//                         ) : null}
            
//                         <label htmlFor="address">Address:</label>
//                         <Field name="address" validate={validateAddress} />
//                         {errors.address && touched.address ? (
//                             <div>{errors.address}</div>
//                         ) : null}
            
//                         <label htmlFor="phoneNumber">Phone number:</label>
//                         <Field name="phoneNumber" validate={validatePhone} />
//                         {errors.phoneNumber && touched.phoneNumber ? <div>{errors.phoneNumber}</div> : null}
            
//                         <button type="submit" className={styles.form__submitBtn}>Checkout</button>
//                     </Form>
//                 )}
//             </Formik>
//         </div>
//     )
// }

// export default CustomerForm;