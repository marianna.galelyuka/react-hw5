import React, { useState } from 'react';
import Modal from '../modal';
import Button from '../button';
import { IoClose } from "react-icons/io5";

import styles from './Item.module.css';

const Item = (props) => {
    const [isModalOpen, setIsModalOpen] = useState(false);

    const changeModal = () => {
        setIsModalOpen(!isModalOpen)
    }

    const deleteItem = () => {
        props.onDeleteFromCart(props.item.id)
    }

    return (
        props.isCloseIcon ?
            <div className={styles.item}>
                <img src={props.item.imgUrl} alt='Item' className={styles.item__img}></img>
                <div className={styles.item__info}>
                    <div className={styles.item__details}>
                        <p className={styles.item__details_name}>{props.item.productName}</p>
                        <p className={styles.item__details_price}>UAH {props.item.price}</p>
                    </div>
                    <IoClose className={styles.item__deleteIcon} onClick={changeModal}/>
                </div>

                {isModalOpen && (
                <div className='Modal' onClick={changeModal}>
                    <Modal header="Are you sure?" text="Do you really want to delete this product from a cart?" actions={<><Button text='Ok' className={styles.product__toCartBtn_confirm} onClick={deleteItem}/><Button text="Cancel" className={styles.product__toCartBtn_cancel} onClick={changeModal} /></>} isOpen={isModalOpen} />
                </div>
                )}
            </div> :
                <div className={styles.item}>
                    <img src={props.item.imgUrl} alt='Item' className={styles.item__img}></img>
                    <div className={styles.item__details}>
                        <p className={styles.item__details_name}>{props.item.productName}</p>
                        <p className={styles.item__details_price}>UAH {props.item.price}</p>
                    </div>
                </div>
    )
}

export default Item;